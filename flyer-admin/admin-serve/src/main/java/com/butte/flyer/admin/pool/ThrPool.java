package com.butte.flyer.admin.pool;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * ThreadPoolExecutor线程池实例
 * @author 公众号:知了一笑
 * @since 2022-03-12 14:46
 */
public class ThrPool implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ThrPool.class) ;
    /**
     * 线程池管理，ThreadFactoryBuilder出自Guava工具库
     */
    private static final ThreadPoolExecutor DEV_POOL;
    static {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("butte-pool-%d").build();
        DEV_POOL = new ThreadPoolExecutor(0, 8,60L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(256),threadFactory, new ThreadPoolExecutor.AbortPolicy());
        DEV_POOL.allowCoreThreadTimeOut(true);
    }
    /**
     * 任务方法
     */
    @Override
    public void run() {
        try {
            logger.info("Print...Job...Run...；queue_size：{}",DEV_POOL.getQueue().size());
            Thread.sleep(5000);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}